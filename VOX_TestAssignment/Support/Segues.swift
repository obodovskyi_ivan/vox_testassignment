//
//  Segues.swift
//  VOX_TestAssignment
//
//  Created by Ivan Obodovskyi on 9/9/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//

import Foundation

struct Segues {
  static let historyTableViewSegue: String = "historySegue"
}
