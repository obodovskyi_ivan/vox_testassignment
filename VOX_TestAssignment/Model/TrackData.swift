  //
//  TrackDAta.swift
//  VOX_TestAssignment
//
//  Created by Ivan Obodovskyi on 9/9/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//

import Foundation

  struct TrackData: Codable {
    let title: String
    let artist: ArtistData
    let album: AlbumData
    
    enum CodingKeys: String, CodingKey {
      case title
      case artist = "artist"
      case album = "album"
    }
    
    func encode(to encoder: Encoder) throws {
      var container = encoder.container(keyedBy: CodingKeys.self)
      try container.encode(title, forKey: .title)
      try container.encode(artist, forKey: .artist)
      try container.encode(album, forKey: .album)
    }
    
    init() {
      self.title = ""
      self.artist = ArtistData()
      self.album = AlbumData()
    }
    
    init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
      title = try container.decodeIfPresent(String.self, forKey: .title) ?? ""
      artist = try container.decodeIfPresent(ArtistData.self, forKey: .artist) ?? ArtistData()
      album = try container.decodeIfPresent(AlbumData.self, forKey: .album) ?? AlbumData()
    }
  }
  
