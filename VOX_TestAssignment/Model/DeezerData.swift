//
//  DeezerData.swift
//  VOX_TestAssignment
//
//  Created by Ivan Obodovskyi on 9/9/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//

import Foundation

struct DeezerData: Codable {
  let data: [TrackData]
  let total: Int
  let next: String
  
  enum CodingKeys: String, CodingKey {
    case data
    case total
    case next
  }
  
  func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    try container.encode(data, forKey: .data)
    try container.encode(total, forKey: .total)
    try container.encode(next, forKey: .next)
  }
  
  internal init() {
    self.data = [TrackData]()
    self.total = 0
    self.next = ""
  }
  
  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    data = try container.decodeIfPresent([TrackData].self, forKey: .data) ?? [TrackData]()
    total = try container.decodeIfPresent(Int.self, forKey: .total) ?? 0
    next =  try container.decodeIfPresent(String.self, forKey: .next) ?? ""
  }
}
