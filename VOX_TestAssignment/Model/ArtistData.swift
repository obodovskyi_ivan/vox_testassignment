//
//  ArtistData.swift
//  VOX_TestAssignment
//
//  Created by Ivan Obodovskyi on 9/9/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//

import Foundation


struct ArtistData: Codable  {
  let name: String
  let largePicture: String
  
  enum CodingKeys: String, CodingKey {
    case name
    case largePicture = "picture_xl"
  }
  
  func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    try container.encode(name, forKey: .name)
    try container.encode(largePicture, forKey: .largePicture)
  }
  
  init() {
    self.name = ""
    self.largePicture = ""
  }
  
  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
    largePicture = try container.decodeIfPresent(String.self, forKey: .largePicture) ?? ""
  }
}


