//
//  AlbumData.swift
//  VOX_TestAssignment
//
//  Created by Ivan Obodovskyi on 9/9/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//

import Foundation

struct AlbumData: Codable {
  let title: String
  let coverXL: String
  
  enum CodingKeys: String, CodingKey {
    case title
    case coverXL = "cover_xl"
  }
  
  func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    try container.encode(title, forKey: .title)
    try container.encode(coverXL, forKey: .coverXL)
  }
  
  init() {
    self.title = ""
    self.coverXL = ""
  }
  
  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    title = try container.decodeIfPresent(String.self, forKey: .title) ?? ""
    coverXL = try container.decodeIfPresent(String.self, forKey: .coverXL) ?? ""
  }
}
