//
//  CoreDataManager.swift
//  VOX_TestAssignment
//
//  Created by Ivan Obodovskyi on 9/14/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//

import Foundation
import CoreData

fileprivate enum CoreDataKeyPaths: String {
  case date
  case requestName
  case requestURL
  
  var description: String {
    switch self {
    case .date:
      return "date"
    case .requestName:
      return "requestName"
    case .requestURL:
      return "requestURL"
    }
  }
}

class CoreDataManager {
  
  static let shared = CoreDataManager()
  
  lazy var persistentContainer: NSPersistentContainer = {
    let container = NSPersistentContainer(name: "RequestHistoryEntity")
    
    container.loadPersistentStores(completionHandler: { (description, error) in
      if let error = error {
        print(error)
      }
    })
    return container
  }()
  
  
  
  private init() { }
  
  func saveToDataBase() {
    let context = CoreDataManager.shared.persistentContainer.viewContext
    
    if context.hasChanges {
      do {
        try context.save()
      } catch let error {
        print(error.localizedDescription)
      }
    }
  }
  
  func saveRequestData(requestName: String, requestURL: String, date: String) {
    let context = CoreDataManager.shared.persistentContainer.viewContext
    let entity = RequestHistoryEntity.init(context: context)
    
    entity.setValue(requestName, forKey: CoreDataKeyPaths.requestName.description)
    entity.setValue(requestURL, forKey: CoreDataKeyPaths.requestURL.description)
    entity.setValue(date, forKey: CoreDataKeyPaths.date.description)
    
    do {
      try context.save()
    } catch let error as NSError {
      print("Could not save. \(error), \(error.userInfo)")
    }
  }
  
  func fetchFromEntity(entity: String) -> [Any]? {
    let context = CoreDataManager.shared.persistentContainer.viewContext
    let fetchRequest = NSFetchRequest<NSFetchRequestResult>.init(entityName: entity)
    
    do {
      return try context.fetch(fetchRequest)
    } catch let error {
      print(error.localizedDescription)
      return nil
    }
  }
  
  func applicationDocumentsDirectory() {
    if let url = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask).last {
      print(url.absoluteString)
    }
  }
  
}
