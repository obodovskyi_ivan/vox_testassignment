//
//  RequestHistoryEntity+CoreDataProperties.swift
//  VOX_TestAssignment
//
//  Created by Ivan Obodovskyi on 9/14/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//
//

import Foundation
import CoreData


extension RequestHistoryEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<RequestHistoryEntity> {
        return NSFetchRequest<RequestHistoryEntity>(entityName: "RequestHistoryEntity")
    }

    @NSManaged public var requestName: String?
    @NSManaged public var date: String?
    @NSManaged public var requestURL: String?

}
