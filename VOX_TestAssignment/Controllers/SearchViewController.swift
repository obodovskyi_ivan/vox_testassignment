//
//  ViewController.swift
//  VOX_TestAssignment
//
//  Created by Ivan Obodovskyi on 9/9/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//

import UIKit


final class SearchViewController: UIViewController {
  // MARK : - Outlets
  @IBOutlet private weak var searchTextField: UITextField!
  @IBOutlet private weak var coverImageView: UIImageView!
  @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
  
  private var data: DeezerData = DeezerData() {
    didSet {
      DispatchQueue.main.async { [weak self] in
        guard let self = self else { return }
        guard let imageURL = self.data.data.first?.album.coverXL else {
          self.createAlert()
          return
        }
        self.coverImageView.fetchImage(url: imageURL)
        self.navigationItem.title = self.data.data.first?.artist.name
        self.activityIndicator.stopAnimating()
      }
    }
  }
  
  let dataRequestService = DataRequestService.init()
  
  //MARK: - View Controller lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
   CoreDataManager.shared.applicationDocumentsDirectory()
    setUpActivityIndicator()
  }
  
  fileprivate func setUpActivityIndicator() {
    activityIndicator.hidesWhenStopped = true
    activityIndicator.isHidden = true
  }
  
  //MARK: - Action Buttons
  @IBAction func searchButtonPressed(_ sender: Any) {
    guard let search = searchTextField.text, !search.isEmpty else { return }
    sendRequest(searchFor: search.formatStringForSearch())
    CoreDataManager.shared.saveRequestData(requestName: search, requestURL: NetworkService.shared.createSearchRequestString(request: search.formatStringForSearch()), date: getCurrentDate())
    activityIndicator.startAnimating()
  }
  
  @IBAction func historyButtonPressed(_ sender: Any) {
    let vc = storyboard?.instantiateViewController(withIdentifier: "HistoryVC") as! HistoryViewController
    vc.delegate = self
    self.navigationController?.pushViewController(vc, animated: true)
    
  }
  
  private func sendRequest(searchFor: String) {
        NetworkService.shared.performRequest(requestFor: searchFor) { result in
      switch result {
      case .failure(let error):
        print(error.localizedDescription)
      case .success(let data):
        self.decodeData(from: data)
      }
    }
  }
  
  private func decodeData(from requestData: Data){
    do {
        self.data = try JSONDecoder().decode(DeezerData.self, from: requestData)
      print(self.data)
    } catch (let error) {
      print(error)
    }
  }
  
  private func createAlert() {
    let alert = UIAlertController(title: "No images fetched", message: nil, preferredStyle: .alert)
    let ok = UIAlertAction(title: "Ok", style: .destructive, handler: { [weak self] _ in
      self?.setUpActivityIndicator()
      self?.coverImageView.image = UIImage.init(named: "notFound")
    })
    
    alert.addAction(ok)
    present(alert, animated: true, completion: nil)
  }
  
}

extension UIViewController {
  func getCurrentDate() -> String {
    let date = Date()
    let format = DateFormatter()
    format.dateFormat = "yyyy-MM-dd HH:mm:ss"
    return format.string(from: date)
  }
}

extension SearchViewController: HistoryViewControllerDelegate {
  func performRequestFromHistory(url: String) {
    sendRequest(searchFor: url.formatStringForSearch())
  }
}


