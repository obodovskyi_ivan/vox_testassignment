//
//  HistoryViewController.swift
//  VOX_TestAssignment
//
//  Created by Ivan Obodovskyi on 9/9/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//

import UIKit

protocol HistoryViewControllerDelegate: class {
  func performRequestFromHistory(url: String)
}

final class HistoryViewController: UIViewController{
  //MARK: - Outlets
  @IBOutlet private weak var historyTableView: UITableView!
  
  weak var delegate: HistoryViewControllerDelegate?
  
  private var historyData: [RequestHistoryEntity] {
    didSet {
      DispatchQueue.main.async { [weak self] in
        self?.historyTableView.reloadData()
      }
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
     self.historyData =  CoreDataManager.shared.fetchFromEntity(entity: "RequestHistoryEntity") as! [RequestHistoryEntity]
    super.init(coder: aDecoder)
  }
  
  //MARK: - View Controller lifecycle
  override func viewDidLoad() {
        super.viewDidLoad()
    self.historyData =  CoreDataManager.shared.fetchFromEntity(entity: "RequestHistoryEntity") as! [RequestHistoryEntity]

    navigationItem.title = "Search hstory"
  }
  
}

//MARK: - TableViewDataSource
extension HistoryViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return historyData.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = historyTableView.dequeueReusableCell(withIdentifier: HistoryTableViewCell.identifier, for: indexPath) as? HistoryTableViewCell else { return UITableViewCell()}
    
    if let text = historyData[indexPath.row].requestName, let date = historyData[indexPath.row].date
    {
      cell.setUpCell(request: text, date: date)
    }
    return cell
  }
}

extension HistoryViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    guard let requestURL = historyData[indexPath.row].requestName else { return }
    delegate?.performRequestFromHistory(url: requestURL)
    self.navigationController?.popViewController(animated: true)
  }
}


