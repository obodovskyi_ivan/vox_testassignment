//
//  UITableViewCell+Identifier.swift
//  VOX_TestAssignment
//
//  Created by Ivan Obodovskyi on 9/9/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//

import UIKit

extension UITableViewCell {
  static var identifier: String {
    return String(describing: self)
  }
  
  static var nib: UINib {
    return UINib(nibName: identifier, bundle: nil)
  }
}
