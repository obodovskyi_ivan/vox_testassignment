//
//  ImagePicker.swift
//  VOX_TestAssignment
//
//  Created by Ivan Obodovskyi on 9/13/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {
  func fetchImage(url: String) {
    guard let url = URL(string: url) else {
      return
    }
    self.kf.setImage(with: url)
  }
}
