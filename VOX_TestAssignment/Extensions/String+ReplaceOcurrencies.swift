//
//  String+ReplaceOcurrencies.swift
//  VOX_TestAssignment
//
//  Created by Ivan Obodovskyi on 9/13/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//

import Foundation

extension String {
  func formatStringForSearch() -> String{
    var val = self.replacingOccurrences(of: " ", with: "%20")
    val = val.replacingOccurrences(of: ".", with: "%20")
    val = val.replacingOccurrences(of: ",", with: "%20")
    val = val.replacingOccurrences(of: "`", with: "%20")
    val = val.replacingOccurrences(of: "~", with: "%20")
    val = val.replacingOccurrences(of: "/", with: "%20")
    val = val.replacingOccurrences(of: "-", with: "%20")
    return val
  }
}
