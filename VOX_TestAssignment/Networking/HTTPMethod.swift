//
//  HTTPMethod.swift
//  VOX_TestAssignment
//
//  Created by Ivan Obodovskyi on 9/11/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//

import Foundation

public typealias HTTPHeaders = []

public enum HTTPMethod: String {
  case get = "GET"
}

public enum HTTPTask {
  case getRequest
}
