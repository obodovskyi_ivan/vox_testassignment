//
//  NetworkService.swift
//  VOX_TestAssignment
//
//  Created by Ivan Obodovskyi on 9/11/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//

import Foundation


class NetworkService {
  static var shared = NetworkService()
  
  private init() {}
  
  enum NetworkRequestType: String {
    case searchRequest
    
    var description: String {
      switch self {
      case .searchRequest:
        return SearchRequestEndpoints.baseURL + SearchRequestEndpoints.path
      }
    }
  }
  
  enum RequestError: Error {
    case noValidUrl
    case wrongPath
    case noData
    case noInternetConnection
    case errorOccured
    case unknownError
  }
  
  func performRequest(requestFor: String, completion: @escaping (Result<Data, Error>) -> Void) {
    guard let url = URL(string: createSearchRequestString(request: requestFor)) else { return }
    print(url)
    let urlSession = URLSession.shared.dataTask(with: url) { (data, response, error) in
      if error != nil {
        completion(.failure(RequestError.errorOccured))
      }else if let response = response {
        print(response)
      } else if let data = data {
        completion(.success(data))
      }
    }
    urlSession.resume()
  }
  
  func createSearchRequestString(request: String) -> String {
    return SearchRequestEndpoints.baseURL + SearchRequestEndpoints.path + request
  }
}
