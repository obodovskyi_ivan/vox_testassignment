//
//  NetworkEndpoints.swift
//  VOX_TestAssignment
//
//  Created by Ivan Obodovskyi on 9/9/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//

import Foundation

struct SearchRequestEndpoints {
  static var baseURL: String = "https://api.deezer.com/"
  static var path: String = "search?q="
}


