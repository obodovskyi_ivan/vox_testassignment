//
//  HistoryTableViewCell.swift
//  VOX_TestAssignment
//
//  Created by Ivan Obodovskyi on 9/9/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//

import UIKit

final class HistoryTableViewCell: UITableViewCell {
  
  @IBOutlet private weak var searchRequestLabel: UILabel!
  @IBOutlet private weak var requestDateLabel: UILabel!

  func setUpCell(request: String, date: String) {
    self.searchRequestLabel.text = request
    self.requestDateLabel.text = date
  }
  
}
